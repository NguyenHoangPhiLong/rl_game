import os
import pygame
import random
import torch
import numpy as np
from DeepQ_game import DeepQ_RL, Game_env
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)
#os.environ["DISPLAY"]
#os.environ["SDL_VIDEODRIVER"] = "dummy"

# Define constants for the screen width and height
SCREEN_WIDTH = 256
SCREEN_HEIGHT = 256
ACTION = {0:'UP',1:'DOWN',2:'LEFT',3:'RIGHT'}
# Define a player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.player_size = (10, 10)
        self.surf = pygame.Surface(self.player_size)
        self.surf.fill((255, 0, 0))
        self.rect = self.surf.get_rect()
        self.get_player_position()
    # Move the sprite based on user keypresses
    def update(self, pressed_keys):
        if pressed_keys[K_UP]:
            self.rect.move_ip(0, -1)
        if pressed_keys[K_DOWN]:
            self.rect.move_ip(0, 1)
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-1, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(1, 0)

        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT
        self.get_player_position()

    def update_action(self, action):
        step = 2
        if action == 0:
            self.rect.move_ip(0, -step)
        if action == 1:
            self.rect.move_ip(0, step)
        if action == 2:
            self.rect.move_ip(-step, 0)
        if action == 3:
            self.rect.move_ip(step, 0)

        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT
        self.get_player_position()

    def get_player_position(self):
        self.player_position = self.rect.center

class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super(Enemy, self).__init__()
        self.enemy_size = (10,10)
        self.surf = pygame.Surface(self.enemy_size)
        self.surf.fill((255, 255, 255))

        self.enemy_position = (
            random.randint(int(self.enemy_size[0]/2),SCREEN_WIDTH-int(self.enemy_size[0]/2)),
            random.randint(int(self.enemy_size[1]/2), SCREEN_HEIGHT-int(self.enemy_size[1]/2)))
        self.rect = self.surf.get_rect(
            center= self.enemy_position )
        self.steps = [-2,0,2]
    # Move the sprite based on speed
    # Remove the sprite when it passes the left edge of the screen
    def update(self):
        delta_x = random.choice(self.steps)
        delta_y = random.choice(self.steps)
        self.rect.move_ip(delta_x,delta_y)
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT
        self.get_enemy_position()

    def get_enemy_position(self):
        self.enemy_position = self.rect.center

class Goal(pygame.sprite.Sprite):
    def __init__(self):
        super(Goal, self).__init__()
        self.goal_size = (10,10) 
        self.surf = pygame.Surface(self.goal_size)
        self.surf.fill((255, 255, 0))
        self.update_goal_position()
    def update_goal_position(self):
        self.goal_position = (
            random.randint(int(self.goal_size[0]/2),SCREEN_WIDTH-int(self.goal_size[0]/2)),
            random.randint(int(self.goal_size[1]/2), SCREEN_HEIGHT-int(self.goal_size[1]/2)))
        #print("New goal: ",self.goal_position)
        self.rect = self.surf.get_rect(center=self.goal_position)


# =========================================
# Build model Reinforcement Learning
# =========================================

agent = DeepQ_RL(SCREEN_WIDTH,SCREEN_HEIGHT,3)
#agent.build_model()



# Main loop
EPISODES    = 4000
cnt         = 0
global_step = 0
scores, episodes = [], []
for e in range(agent.load_episode + 1, EPISODES):
    # Initialize pygame
    pygame.init()
    # Create the screen object
    # The size is determined by the constant SCREEN_WIDTH and SCREEN_HEIGHT
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

    # Create a custom event for adding a new enemy
    ADDENEMY = pygame.USEREVENT + 1
    pygame.time.set_timer(ADDENEMY, 250)
    # Instantiate player. Right now, this is just a rectangle.
    player = Player()
    # Create goal
    goal   = Goal()
    goals = pygame.sprite.Group()
    goals.add(goal)
    # group
    enemies = pygame.sprite.Group()
    all_sprites = pygame.sprite.Group()

    all_sprites.add(player)
    all_sprites.add(goal)
    # Variable to keep the main loop running
    running = True
    new_enemy1 = Enemy()
    enemies.add(new_enemy1)
    all_sprites.add(new_enemy1)

    new_enemy2 = Enemy()
    enemies.add(new_enemy2)
    all_sprites.add(new_enemy2)

    new_enemy3 = Enemy()
    enemies.add(new_enemy3)
    all_sprites.add(new_enemy3)

    new_enemy4 = Enemy()
    enemies.add(new_enemy4)
    all_sprites.add(new_enemy4)
    game_env = Game_env(SCREEN_WIDTH,SCREEN_HEIGHT,player.player_position, goal.goal_position)

    score    = 0
    win_goal = False
    done     = False
    state = game_env.get_state_env_distance(player.player_position, 
                            [new_enemy1.enemy_position,
                            new_enemy2.enemy_position,
                            new_enemy3.enemy_position,
                            new_enemy4.enemy_position],
                            goal.goal_position)
    t = 0
    while True:
    # for t in range(agent.episode_step):
        action = agent.getAction(state)
        player.update_action(action)
        enemies.update()
        # Fill the screen with black
        screen.fill((0, 0, 0))

        # Draw the player on the screen
        #screen.blit(player.surf, player.rect)
        for entity in all_sprites:
            screen.blit(entity.surf, entity.rect)

        if pygame.sprite.spritecollideany(player, enemies):
            # If so, then remove the player and stop the loop
            player.kill()
            #return 
            running = False
            done = True
        if pygame.sprite.spritecollideany(player, goals):
            win_goal = True
            t=-1
            # Initialize new goal position
            goal.update_goal_position()
            game_env.cal_goal_distance(player.player_position,goal.goal_position)
            
        # Update the display
        pygame.display.flip()
        #=========================
        next_state = game_env.get_state_env_distance(player.player_position, 
                            [new_enemy1.enemy_position,
                            new_enemy2.enemy_position,
                            new_enemy3.enemy_position,
                            new_enemy4.enemy_position],
                            goal.goal_position)
        reward = game_env.get_reward(next_state,done, win_goal, action)
        agent.appendMemory(state, action, reward, next_state, done)

        if len(agent.memory) >= agent.train_start:
            if global_step <= agent.target_update:
                #print('Train model')
                agent.trainModel_distance()
            else:
                agent.trainModel_distance(True)

        score += reward
        state = next_state
        win_goal = False
        if t >= agent.episode_step-10:
            print("Time out!!")
            done = True

        if done:
            agent.updateTargetModel()
            print('Ep: %d score: %.2f memory: %d epsilon: %.2f loss_mse: %.2f'%
                            (e, score, len(agent.memory), agent.epsilon, agent.loss_train))
            break
        global_step += 1
        t+=1
        if t == agent.episode_step:
            print('============ End episode ==========')
            break
        #if global_step % agent.target_update == 0:
            #print("Update target network")
    if agent.epsilon > agent.epsilon_min:
        agent.epsilon *= agent.epsilon_decay
    
