import numpy as np
import torch
import random
from torchsummary import summary
from torchvision import models
from collections import deque

class DeepQ_RL():
    def __init__(self,width, height, depth):
        self.state_size = depth*width*height
        self.state_size_distance = 13
        self.depth = depth
        self.width = width
        self.height= height
        self.load_episode = 0
        self.episode_step = 3000
        self.target_update = 1500
        self.batch_size = 8
        self.train_start = 64
        self.layer1 = 100
        self.layer2 = 150
        self.action_size = 4
        self.learning_rate = 1e-3
        self.discount_factor = 0.99
        self.gamma = 0.9
        self.epsilon = 1.0
        self.epsilon_decay = 0.99
        self.epsilon_min = 0.05
        self.memory = deque(maxlen=1000000)
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu") 
        print("Device: ",self.device)
        self.loss_train = 0
        self.model = self.build_model_v2()
        self.target_model = self.build_model_v2()
        self.updateTargetModel()

    def build_model(self):
        model = torch.nn.Sequential(
        torch.nn.Conv2d(self.depth, 4, kernel_size=3, stride=2, padding = 1),
        torch.nn.ReLU(),
        torch.nn.ZeroPad2d((0,2,2,0)),
        torch.nn.Conv2d(4, 4, kernel_size=3, stride=2),
        torch.nn.ReLU(),
        torch.nn.ZeroPad2d((0,2,2,0)),
        torch.nn.Conv2d(4, 4, kernel_size=3, stride=2),
        torch.nn.ReLU(),
        torch.nn.MaxPool2d(2, stride=2),
        torch.nn.Flatten(),
        torch.nn.Linear(256, self.layer1),
        torch.nn.ReLU(),
        torch.nn.Linear(self.layer1, self.layer2),
        torch.nn.ReLU(),
        torch.nn.Linear(self.layer2, self.action_size)
        )
        self.loss_fn = torch.nn.MSELoss()
        
        self.optimizer = torch.optim.Adam(model.parameters(), lr=self.learning_rate)   
        model.to(self.device)     
        summary(model,(self.depth,self.width,self.height))
        return model

    def build_model_v2(self):
        model = torch.nn.Sequential(
        torch.nn.Linear(self.state_size_distance, self.layer1),
        torch.nn.ReLU(),
        torch.nn.Linear(self.layer1, self.layer2),
        torch.nn.ReLU(),
        torch.nn.Linear(self.layer2, self.action_size)
        )
        # model = model.float()
        self.loss_fn = torch.nn.MSELoss()
        
        self.optimizer = torch.optim.RMSprop(model.parameters(), lr=self.learning_rate)   

        model.to(self.device)     
        summary(model,(self.state_size_distance,))
        return model

    def getQvalue(self, reward, next_target, done):
        if done:
            return reward
        else:
            return reward + self.discount_factor * np.amax(next_target)

    def updateTargetModel(self):
        #self.target_model.set_weights(self.model.get_weights())
        self.target_model.load_state_dict(self.model.state_dict()) 

    def getAction(self, state):
        if np.random.rand() <= self.epsilon:
            self.q_value = np.zeros(self.action_size)
            return random.randrange(self.action_size)
        else:
            state_torch = torch.tensor(np.expand_dims(state,axis=0)).to(self.device)
            q_value = self.model(state_torch)
            self.q_value = q_value.cpu().detach().numpy()
            return np.argmax(self.q_value[0])

    def appendMemory(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))
    
    def trainModel(self, target=False):
        mini_batch = random.sample(self.memory, self.batch_size)
        X_batch = np.empty((0, self.depth,self.width, self.height), dtype=np.float32)
        Y_batch = np.empty((0, self.action_size), dtype=np.float32)
        
        for i in range(self.batch_size):
            states = mini_batch[i][0]
            actions = mini_batch[i][1]
            rewards = mini_batch[i][2]
            next_states = mini_batch[i][3]
            dones = mini_batch[i][4]
            states_torch = torch.tensor(np.expand_dims(states,axis=0)).to(self.device)
            next_states_torch = torch.tensor(np.expand_dims(next_states,axis=0)).to(self.device)
             
            q_value = self.model(states_torch).cpu().detach().numpy()
            self.q_value = q_value
            if target:
                next_target = self.target_model(next_states_torch).cpu().detach().numpy()
            else:
                next_target = self.model(next_states_torch).cpu().detach().numpy()

            next_q_value = self.getQvalue(rewards, next_target, dones)
            #print("Next_q_value",next_q_value)
            X_batch = np.append(X_batch, np.array([states.copy()]), axis=0)
            Y_sample = q_value.copy()

            Y_sample[0][actions] = next_q_value
            Y_batch = np.append(Y_batch, np.array([Y_sample[0]]), axis=0)

            if dones:
                X_batch = np.append(X_batch, np.array([next_states.copy()]), axis=0)
                Y_batch = np.append(Y_batch, np.array([[rewards] * self.action_size]), axis=0)
    
        X_batch_tensor = torch.tensor(X_batch).to(self.device)
        Y_batch_tensor = torch.tensor(Y_batch).to(self.device)
        # self.model.fit(X_batch, Y_batch, batch_size=self.batch_size, epochs=1, verbose=0)
        # zero the parameter gradients
        self.optimizer.zero_grad()

        # forward + backward + optimize
        outputs = self.model(X_batch_tensor)
        loss = self.loss_fn(outputs, Y_batch_tensor)
        self.optimizer.step()
    def trainModel_distance(self, target=False):
        mini_batch = random.sample(self.memory, self.batch_size)
        X_batch = np.empty((0, self.state_size_distance), dtype=np.float32)
        Y_batch = np.empty((0, self.action_size), dtype=np.float32)

        for i in range(self.batch_size):
            states = mini_batch[i][0]
            actions = mini_batch[i][1]
            rewards = mini_batch[i][2]
            next_states = mini_batch[i][3]
            dones = mini_batch[i][4]
            states_torch = torch.tensor(np.expand_dims(states,axis=0)).to(self.device)
            next_states_torch = torch.tensor(np.expand_dims(next_states,axis=0)).to(self.device)
             
            q_value = self.model(states_torch).cpu().detach().numpy()
            self.q_value = q_value
            if target:
                next_target = self.target_model(next_states_torch).cpu().detach().numpy()
            else:
                next_target = self.model(next_states_torch).cpu().detach().numpy()

            next_q_value = self.getQvalue(rewards, next_target, dones)
            #print("Next_q_value",next_q_value)
            X_batch = np.append(X_batch, np.array([states.copy()]), axis=0)
            Y_sample = q_value.copy()
    
            Y_sample[0][actions] = next_q_value
            Y_batch = np.append(Y_batch, np.array([Y_sample[0]]), axis=0)

            if dones:
                X_batch = np.append(X_batch, np.array([next_states.copy()]), axis=0).astype('float32')
                Y_batch = np.append(Y_batch, np.array([[rewards] * self.action_size]), axis=0).astype('float32')
    
        X_batch_tensor = torch.tensor(X_batch).to(self.device)
        Y_batch_tensor = torch.tensor(Y_batch).to(self.device)
        # self.model.fit(X_batch, Y_batch, batch_size=self.batch_size, epochs=1, verbose=0)
        # zero the parameter gradients
        self.optimizer.zero_grad()
        outputs = self.model(X_batch_tensor)
        # print(outputs.type(), Y_batch_tensor.type())
        self.loss_train = self.loss_fn(outputs, Y_batch_tensor)
        self.loss_train.backward()
        self.optimizer.step()

class Game_env():
    def __init__(self, env_width, env_height,player_position, goal_position):
        self.env_width = env_width
        self.env_height = env_height
        self.min_range = 15.0
        self.min_goal_dis = 15.0
        self.cal_goal_distance(player_position, goal_position)

    def cal_goal_distance(self,player_pos,goal_pos):
        self.goal_distance =  self.cal_distance(player_pos, goal_pos)

    def cal_distance(self,pointA, pointB):
        distance = np.linalg.norm(np.asarray(pointA)-np.asarray(pointB))
        return distance.astype('float32')
    
    def get_state_env_distance(self, player_pos, enemies_pos, goal_pos):
        state = np.zeros((1,len(enemies_pos)+9)).astype('float32')
        state[0,0] = int(self.env_height- player_pos[1])    # Up
        state[0,1] = int(player_pos[1])                     # DOWN
        state[0,2] = int(self.env_width - player_pos[0])    # LEFT
        state[0,3] = int(player_pos[0])                     # RIGHT

        
        state[0,4] = self.cal_distance(player_pos,(0,0))
        state[0,5] = self.cal_distance(player_pos,(self.env_width,0))
        state[0,6] = self.cal_distance(player_pos,(self.env_width,self.env_height))
        state[0,7] = self.cal_distance(player_pos,(0,self.env_height))

        self.current_distance = self.cal_distance(player_pos, goal_pos)
        state[0,-1]= self.current_distance
        for i,enemy_pos in enumerate(enemies_pos):
            state[0,i+8] = self.cal_distance(player_pos, enemy_pos)
        self.obstacle_distance = min(list(state[0,4:8]))
        return np.squeeze(state,axis=0)
    
    def get_state_env_pos(self,player_pos, enemies_pos, goal_pos):
        dis_player2enemy = []
        state = np.zeros((3,self.env_width, self.env_height)).astype('float32')
        state[0,player_pos[0], player_pos[1]] = 1
        state[2,goal_pos[0], goal_pos[1]] = 1
        
        for i,enemy_pos in enumerate(enemies_pos):
            state[1,enemy_pos[0], enemy_pos[1]] = 1
            dis_player2enemy.append(self.cal_distance(player_pos, enemy_pos))
        
        self.current_distance = self.cal_distance(player_pos, goal_pos)
        self.obstacle_distance = min(dis_player2enemy)

        return state

    def get_reward(self, state, done, win_goal, action):
        distance_rate = (2 ** (self.current_distance/self.goal_distance))
        
        if self.obstacle_distance < 20:
            ob_reward = -5
        else:
            ob_reward = 0
        
        reward = (state[action]/self.env_height)*distance_rate + ob_reward
        
        if done:
            print("Collision!!")
            reward = -500

        if win_goal:
            print("Goal!!")
            reward = 1000
        return reward
